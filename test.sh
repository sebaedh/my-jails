#!/bin/sh

error() { echo -n "Error: $1\nExiting...\n"; exit 1; }

# Check if root
# if ! [ "$(id -u)" = 0 ]; then error "Must be root!"; fi;

iocageExec() 
{
  if ! iocage exec "$1"; then
    error "Iocage command execution failed"
  fi
}

iocageCreate() 
{
  if ! iocage create "$1"; then
    error "Iocage create failed"
  fi
}

iocageFstab()
{
  if ! iocage fstab -a "$1"; then
    error "Iocage fstab failed"
   fi
}

TIME_ZONE="Europe/Stockholm"

JAIL_R="11.3-RELEASE"
HOST_NAME="sebb.io"
CLOUD_DIR="/mnt/odessa/cloud_data"
MOVIE_DIR="/mnt/odessa/jails_data/movies"
SERIES_DIR="/mnt/odessa/jails_data/series"
MUSIC_DIR="/mnt/odessa/jails_data/music"
DB_IP="192.168.20.40/24"
NEXTCLOUD_IP="192.168.20.50/24"
DB_ROOT_PASSWORD=$(openssl rand -base64 16)
DB_PASSWORD=$(openssl rand -base64 16)
ADMIN_PASSWORD=$(openssl rand -base64 12)

JAILS="nextcloud mariadb proxy plex trans1"
j0="nextcloud"
j1="mariadb"
j2="proxy"
j3="plex"
j4="trans1"
i=0
y=4

while [ $i -lt $y ]
do
	eval echo "${i}  ${i}"
	let i=$i+1
done
