#!/bin/sh

iocageExec() {
  if ! iocage exec $1; then
    error "Iocage command execution failed"
  fi
}

iocageFstab() {
  if ! iocage fstab -a $1; then
    error "Iocage fstab failed"
   fi
}
DB_ROOT_PASSWORD=$(openssl rand -base64 16)
DB_PASSWORD=$(openssl rand -base64 16)
ADMIN_PASSWORD=$(openssl rand -base64 12)


iocage fstab -a mariadb "${DB_DATA}" /var/db/mysql nullfs rw 0 0

#### CREATE THE JAILS
iocage exec nextcloud sysrc redis_enable="YES"
iocage exec nextcloud sysrc php_fpm_enable="YES"
iocage exec nextcloud portsnap fetch extract
iocage exec nextcloud sh -c "ALLOW_UNSUPPORTED_SYSTEM=1 make -C /usr/ports/lang/php73 clean reinstall BATCH=yes"
iocage exec nextcloud sh -c "ALLOW_UNSUPPORTED_SYSTEM=1 make -C /usr/ports/databases/pecl-redis clean install BATCH=yes"
iocage exec nextcloud sh -c "ALLOW_UNSUPPORTED_SYSTEM=1 make -C /usr/ports/devel/pecl-APCu clean install BATCH=yes"

iocage exec mariadb pkg update -f
iocage exec mariadb pkg upgrade -yf
iocage exec mariadb pkg install -qy mariadb103-server php73-pdo_mysql php73-mysqli
iocage exec mariadb sysrc mysql_enable="YES"

iocage exec mariadb mysql -u root -e "CREATE DATABASE nextcloud;"
iocage exec mariadb mysql -u root -e "GRANT AL ON nextcloud.* TO nextcloud@192.168.20.50 IDENTIFIED BY '${DB_PASSWORD}';"
iocage exec mariadb mysql -u root -e "DELETE FROM mysql.user WHERE User='';"
iocage exec mariadb mysql -u root -e "DELETE FROM mysql.user WHERE User='root AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
iocage exec mariadb mysql -u root -e "DROP DATABASE IF EXISTS test;"
iocage exec mariadb mysql -u root -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';"
iocage exec mariadb mysqladmin reload
iocage exec mariadb sed -i '' "s|mypassword|${DB_ROOT_PASSWORD}|" /root/.my.cnf

iocage exec mariadb echo "${DB_NAME} root password is ${DB_ROOT_PASSWORD}" > /root/db_password.txt
iocage exec mariadb echo "Nextcloud database password is ${DB_PASSWORD}" >> /root/db_password.txt
iocage exec mariadb echo "Nextcloud Administrator password is ${ADMIN_PASSWORD}" >> /root/db_password.txt

iocage exec nextcloud su -m www -c "php /usr/local/www/nextcloud/occ maintenance:install --database=\"mysql\" --database-name=\"nextcloud\" --database-user=\"nextcloud\" --database-pass=\"${DB_PASSWORD}\" --database-host=\"${DB_IP}:3306\" --admin-user=\"admin\" --admin-pass=\"${ADMIN_PASSWORD}\" --data-dir=\"/mnt/files\""
iocage exec nextcloud su -m www -c "php /usr/local/www/nextcloud/occ config:system:set mysql.utf8mb4 --type boolean --value=\"true\""
iocage exec nextcloud su -m www -c "php /usr/local/www/nextcloud/occ config:system:set logtimezone --value=\"${TIME_ZONE}\""
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set log_type --value="file"'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set logfile --value="/var/log/nextcloud.log"'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set loglevel --value="2"'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set logrotate_size --value="104847600"'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set memcache.local --value="\OC\Memcache\APCu"'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set redis host --value="/tmp/redis.sock"'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set redis port --value=0 --type=integer'
iocage exec nextcloud su -m www -c 'php /usr/local/www/nextcloud/occ config:system:set memcache.locking --value="\OC\Memcache\Redis"'
