#!/bin/sh

error() { echo -n "Error: $1\nExiting..."; exit 1; }

iocageFstab() {
	if [ "$#" -nq 3]; then error "USAGE: $0 <jail> <src> <dst>"; fi;
	jail="$1"
	src="$2"
	dst="$3"
	if ! iocage fstab -a "$jail" "$src" "$dst" nullfs rw 0 0 ; then
		error "Iocage fstab failed with jail: $jail src: $src dst: $dst"
	fi
}

if ! [ "$(id -u)" = 0 ]; then error "Must be root!"; fi;

# IPS
DB_IP="192.168.20.20/24"
NEXTCLOUD_IP="192.168.20.50/24"
PROXY_IP="192.168.20.10/24"
PLEX_IP="192.168.20.90/24"
TRANS1_IP="192.168.20.11/24"
TRANS2_IP="192.168.20.12/24"

INTERFACES="vnet0:bridge20"

### GENERAL VARIABLES
JAIL_R="11.3-RELEASE"
HOST_NAME="sebb.io"
CLOUD_DIR="/mnt/odessa/cloud_data"
DB_DIR="/mnt/odessa/db"
MOVIE_DIR="/mnt/odessa/jails_data/movies"
SERIES_DIR="/mnt/odessa/jails_data/series"
MUSIC_DIR="/mnt/odessa/jails_data/music"

TRANS1_DOWN_DIR="/mnt/odessa/jails_data/trans1_download"
TRANS2_DOWN_DIR="/mnt/odessa/jails_data/trans2_download"

### JAIL NAMES
NEXTCLOUD="nextcloud"
MARIADB="maridb"
PROXY="proxy"
PLEX="plex"
TRANS1="trans1"
TRANS2="trans2"

cat <<__EOF__ >/tmp/pkg.json
{
	"pkgs":[ "vim","sudo","gnupg","bash","p5-Locale-gettext","help2man","texinfo","m4","autoconf","python27","python" ]
}
__EOF__

iocage create --name "${NEXTCLOUD}" -p /tmp/pkg.json -r "${JAIL_R}" \
	ip4_addr="${NEXTCLOUD_IP}" defaultrouter="192.168.20.1" boot="on" \
	host_hostname=""${NEXTCLOUD}"" vnet=on interfaces="$INTERFACES" bpf=yes

iocage create --name "${MARIADB}" -p /tmp/pkg.json -r "${JAIL_R}" \
	ip4_addr="${DB_IP}" defaultrouter="192.168.20.1" boot="on" \
	host_hostname="${MARIADB}" vnet=on interfaces="$INTERFACES" bpf=yes

iocage create --name "${PLEX}" -p /tmp/pkg.json -r "${JAIL_R}" \
	ip4_addr="${PLEX_IP}" defaultrouter="192.168.20.1" boot="on" \
	host_hostname="${PLEX}" vnet=on interfaces="$INTERFACES" bpf=yes

iocage create --name "${PROXY}" -p /tmp/pkg.json -r "${JAIL_R}" \
	ip4_addr="${PROXY_IP}" defaultrouter="192.168.20.1" boot="on" \
	host_hostname="${PROXY}" vnet=on interfaces="$INTERFACES" bpf=yes

iocage create --name "${TRANS1}" -p /tmp/pkg.json -r "${JAIL_R}" \
	ip4_addr="${TRANS1_IP}" defaultrouter="192.168.20.1" boot="on" \
	host_hostname="${TRANS1}" vnet=on interfaces="$INTERFACES" bpf=yes

iocage create --name "${TRANS2}" -p /tmp/pkg.json -r "${JAIL_R}" \
	ip4_addr="${TRANS2_IP}" defaultrouter="192.168.20.1" boot="on" \
	host_hostname="${TRANS2}" vnet=on interfaces="$INTERFACES" bpf=yes

#### MOUNT DRIVES
# iocage fstab -a $JAIL $SRC $DST nullfs rw 0 0

mkdir -p /mnt/odessa/iocage/jails/${NEXTCLOUD}/root/mnt/cloud_data
iocage fstab -a "${NEXTCLOUD}" "${CLOUD_DIR}" "/mnt/cloud_data" nullfs rw 0 0

mkdir -p /mnt/odessa/iocage/jails/${MARIADB}/root/var/db/mysql
iocage fstab -a "${MARIDB}" "${DB_DIR}" "/var/db/mysql" nullfs rw 0 0

mkdir -p /mnt/odessa/iocage/jails/${PLEX}/root/mnt/movies
mkdir -p /mnt/odessa/iocage/jails/${PLEX}/root/mnt/series
mkdir -p /mnt/odessa/iocage/jails/${PLEX}/root/mnt/music
iocage fstab -a "${PLEX}" "${MOVIE_DIR}" "/mnt/movies" nullfs rw 0 0
iocage fstab -a "${PLEX}" "${SERIES_DIR}" "/mnt/series" nullfs rw 0 0
iocage fstab -a "${PLEX}" "${MUSIC_DIR}" "/mnt/music" nullfs rw 0 0

mkdir -p /mnt/odessa/iocage/jails/${TRANS1}/root/mnt/downloads
mkdir -p /mnt/odessa/iocage/jails/${TRANS1}/root/mnt/movies
mkdir -p /mnt/odessa/iocage/jails/${TRANS1}/root/mnt/series
iocage fstab -a "${TRANS1}" "${TRANS1_DOWN_DIR}" "/mnt/downloads" nullfs rw 0 0
iocage fstab -a "${TRANS1}" "${MOVIE_DIR}" "/mnt/movies" nullfs rw 0 0
iocage fstab -a "${TRANS1}" "${SERIES_DIR}" "/mnt/series" nullfs rw 0 0

mkdir -p /mnt/odessa/iocage/jails/${TRANS2}/root/mnt/downloads
iocage fstab -a "${TRANS2}" "${TRANS2_DOWN_DIR}" "/mnt/downloads" nullfs rw 0 0
